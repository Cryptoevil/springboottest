package com.ykoval.test.repository.db;

public interface DataProvider<T> {

  T getInstance();
}
