package com.ykoval.test.repository;

import com.ykoval.test.model.pojo.Card;
import com.ykoval.test.model.pojo.User;
import com.ykoval.test.repository.db.DataProvider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;

public class DBRepository implements DataRepository {

  private DataProvider<Connection> provider;

  public DBRepository(@NotNull DataProvider<Connection> provider) {
    this.provider = provider;
  }

  @Override
  public boolean addUser(User user) {
    Connection connection = this.provider.getInstance();
    if (connection == null) {
      return false;
    }

    StringBuilder sb = new StringBuilder()
        .append("INSERT INTO users(username, user_id)")
        .append(" VALUES ('")
        .append(user.getUsername())
        .append("','")
        .append(user.getUserId())
        .append("')");

    try {
      Statement statement = connection.createStatement();
      statement.executeUpdate(sb.toString());
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  @Override
  public User getUser(String username) {
    Connection connection = this.provider.getInstance();
    if (connection == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder()
        .append("SELECT * FROM users ")
        .append("WHERE username=")
        .append("'")
        .append(username)
        .append("'");

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(sb.toString());

      String userId = null;
      while (resultSet.next()) {
        userId = resultSet.getString("user_id");
      }
      if (userId == null || userId.isEmpty()) {
        return null;
      }
      return new User(username, userId);

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public List<User> getUsers() {
    Connection connection = this.provider.getInstance();
    if (connection == null) {
      return null;
    }
    List<User> users = new ArrayList<>();

    String query = "SELECT * FROM users";

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(query);

      while (resultSet.next()) {
        users.add(new User(resultSet.getString("username"), resultSet.getString("user_id")));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return users;
  }

  @Override
  public Card getCard(int uniqueId) {
    Connection connection = this.provider.getInstance();
    if (connection == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder()
        .append("SELECT * FROM cards ")
        .append("WHERE c_unique_id=")
        .append(uniqueId);

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(sb.toString());

      Card card = null;
      while (resultSet.next()) {
        String cTitle = resultSet.getString("c_title");
        String cDescription = resultSet.getString("c_description");
        if (cTitle != null && !cTitle.isEmpty()) {
          card = new Card(cTitle, cDescription, uniqueId);
        }
      }
      return card;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public List<Card> getCards() {
    return null;
  }
}
