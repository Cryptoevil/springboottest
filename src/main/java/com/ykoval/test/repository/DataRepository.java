package com.ykoval.test.repository;

import com.ykoval.test.model.pojo.Card;
import com.ykoval.test.model.pojo.User;
import java.util.List;

public interface DataRepository {

  boolean addUser(User user);

  User getUser(String username);

  List<User> getUsers();

  Card getCard(int uniqueId);

  List<Card> getCards();
}
