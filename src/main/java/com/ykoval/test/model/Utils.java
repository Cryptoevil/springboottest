package com.ykoval.test.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {

  public static String toMD5(String string) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] array = md.digest(string.getBytes());
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < array.length; ++i) {
        sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
      }

      return sb.toString();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return null;
  }
}
