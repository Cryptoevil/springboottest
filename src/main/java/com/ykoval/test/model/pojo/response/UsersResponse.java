package com.ykoval.test.model.pojo.response;

import com.ykoval.test.model.pojo.User;
import java.util.List;

public class UsersResponse extends BaseResponse {

  private List<User> users;

  public UsersResponse(Head head) {
    super(head);
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }
}
