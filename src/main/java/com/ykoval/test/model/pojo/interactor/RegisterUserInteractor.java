package com.ykoval.test.model.pojo.interactor;

import com.ykoval.test.model.Utils;
import com.ykoval.test.model.pojo.User;
import com.ykoval.test.model.pojo.response.BaseResponse;
import com.ykoval.test.model.pojo.response.BaseResponse.Head;
import com.ykoval.test.model.pojo.response.ErrorResponse;
import com.ykoval.test.model.pojo.response.RegisterResponse;
import com.ykoval.test.repository.DataRepository;
import io.reactivex.ObservableOnSubscribe;
import org.springframework.web.context.request.async.DeferredResult;

public class RegisterUserInteractor extends BaseInteractor<String, BaseResponse, User> {

  public RegisterUserInteractor(DataRepository repository) {
    super(repository);
  }

  @Override
  ObservableOnSubscribe<User> interactorSource(String username) {
    return emitter -> {
      if (username != null && !username.isEmpty()) {

        User user = new User();
        String userId = Utils.toMD5(username);

        user.setUsername(username);
        user.setUserId(userId);

        if (isRegistered(user)) {
          if (!emitter.isDisposed()) {
            emitter.onError(new Exception("User with this username already exist."));
          }
          return;
        }

        boolean result = repository.addUser(user);
        if (!emitter.isDisposed()) {
          if (result) {
            emitter.onNext(user);
          } else {
            emitter.onError(new Exception("Unexpected error."));
          }
        }
      } else {
        if (!emitter.isDisposed()) {
          emitter.onError(new Exception("Invalid parameter."));
        }
      }
    };
  }

  private boolean isRegistered(User user) {
    User oldUser = repository.getUser(user.getUsername());
    return oldUser != null;
  }

  @Override
  void onSuccess(User user, DeferredResult<BaseResponse> result) {
    Head head = new Head(System.currentTimeMillis());
    RegisterResponse response = new RegisterResponse(head);
    response.setUserId(user.getUserId());
    response.setUsername(user.getUsername());
    response.setInfo("New user registered.");

    result.setResult(response);
  }

  @Override
  void onFail(Throwable e, DeferredResult<BaseResponse> result) {
    Head head = new Head(System.currentTimeMillis());
    ErrorResponse response = new ErrorResponse(head, e.getMessage());

    result.setResult(response);
  }
}
