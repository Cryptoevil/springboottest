package com.ykoval.test.model.pojo.interactor;

import com.ykoval.test.repository.DataRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import org.springframework.web.context.request.async.DeferredResult;

public abstract class BaseInteractor<A, R, M>  {

  private final CompositeDisposable disposables;
  protected final DataRepository repository;

  public BaseInteractor(DataRepository repository) {
    this.repository = repository;
    this.disposables = new CompositeDisposable();
  }

  public final void execute(DeferredResult<R> result, A arg) {
    DisposableObserver<M> observer = Observable.create(interactorSource(arg))
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribeWith(new DisposableObserver<M>() {
          @Override
          public void onNext(M model) {
            onSuccess(model, result);
          }

          @Override
          public void onError(Throwable e) {
            onFail(e, result);
          }

          @Override
          public void onComplete() {

          }
        });
    this.disposables.add(observer);
  }

  abstract ObservableOnSubscribe<M> interactorSource(A arg);

  abstract void onSuccess(M model, DeferredResult<R> result);

  abstract void onFail(Throwable e, DeferredResult<R> result);

  public void dispose() {
    this.disposables.dispose();
  }
}
