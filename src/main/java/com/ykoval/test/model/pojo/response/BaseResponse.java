package com.ykoval.test.model.pojo.response;

public abstract class BaseResponse {

  private final Head head;

  public BaseResponse(Head head) {
    this.head = head;
  }

  public Head getHead() {
    return head;
  }


  public static class Head {

    private final long date;

    public Head(long date) {
      this.date = date;
    }

    public long getDate() {
      return date;
    }
  }
}
