package com.ykoval.test.model.pojo.response;

import com.ykoval.test.model.pojo.Card;
import java.util.List;

public class CardsResponse extends BaseResponse {

  private String userId;
  private List<Card> cards;

  public CardsResponse(Head head) {
    super(head);
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public List<Card> getCards() {
    return cards;
  }

  public void setCards(List<Card> cards) {
    this.cards = cards;
  }
}
