package com.ykoval.test.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ErrorControllerImpl implements ErrorController {

  @RequestMapping("/error")
  public String handleError(HttpServletRequest request) {
    String errorMsg = "";
    int httpErrorCode = (int) request
        .getAttribute("javax.servlet.error.status_code");

    switch (httpErrorCode) {
      case 400: {
        errorMsg = "Http Error Code: 400. Bad Request";
        break;
      }
      case 401: {
        errorMsg = "Http Error Code: 401. Unauthorized";
        break;
      }
      case 404: {
        errorMsg = "Http Error Code: 404. Resource not found";
        break;
      }
      case 500: {
        errorMsg = "Http Error Code: 500. Internal Server Error";
        break;
      }
    }
    return errorMsg;
  }

  @Override
  public String getErrorPath() {
    return "/error";
  }
}
